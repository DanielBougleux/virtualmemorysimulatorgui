package com.os_emulator;

public class Configuration {
    //Guarda valores de entrada da janela de configuração
    private int tam_mem_principal;
    private int tam_mem_secundaria;
    private int tam_end_logico;
    private int tam_do_quadro;
    //E suas respectivas unidades
    private char u_mp;
    private char u_disco;
    private char u_quadro;
    //Política de substituição
    private String politica_substituicao;

    //SETTERS PARA OS TAMANHOS
    public void setTamMP(int tam_mp) {
        this.tam_mem_principal = tam_mp;
    }
    public void setTamMDisco(int tam_mdisco) {
        this.tam_mem_secundaria = tam_mdisco;
    }
    public void setTamEnd(int tam_end) {
        this.tam_end_logico = tam_end;
    }
    public void setTamQuad(int tam_quad) {
        this.tam_do_quadro = tam_quad;
    }

    //SETTERS DAS UNIDADES DOS ATRIBUTOS
    public void setU_mp(char u_mp) {
        this.u_mp = u_mp;
    }
    public void setU_disco(char u_disco) {
        this.u_disco = u_disco;
    }
    public void setU_quadro(char u_quadro) {
        this.u_quadro = u_quadro;
    }

    //SETTER PARA POLITICA DE SUBSTITUICAO
    public void setPoliticaSubstituicao(String pol) { this.politica_substituicao = pol; }

    //GETTER PARA AS UNIDADES
    public char getU_mp() { return u_mp; }
    public char getU_disco() { return u_disco; }
    public char getU_quadro() { return u_quadro; }

    //GETTER PARA POLITICA DE SUBSTITUICAO
    public boolean getPoliticaSubstituicao() {
        return politica_substituicao.equalsIgnoreCase("LRU");
    }

    //CONVERSOR DE UNIDADES PARA MB
    private int converteMb(int quantidade, char u){
        if(u == 'b'){
            return quantidade/1048576;
        }else if(u == 'k'){
            return quantidade/1024;
        }else if(u == 'm'){
            return quantidade;
        }else if(u == 'g'){
            return quantidade * 1024;
        }
        return 1;
    }

    //GETTER PARA OS TAMANHOS
    public int getTamMemPrincipal(){
        return converteMb(tam_mem_principal, u_mp);
    }
    public int getTamMemSecundaria(){
        return converteMb(tam_mem_secundaria, u_disco);
    }
    public int getTamEndLogico(){
        return tam_end_logico;
    }
    //Converte tamanho do quadro em KB
    public int getTamQuadro(){
        if(u_quadro == 'b'){
            return tam_do_quadro/1024;
        }else if(u_quadro == 'k'){
            return tam_do_quadro;
        }else if(u_quadro == 'm'){
            return tam_do_quadro * 1024;
        }else if(u_quadro == 'g'){
            return tam_do_quadro * 1048576;
        }
        return 1;
    }
}
