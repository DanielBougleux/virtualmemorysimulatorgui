package com.os_emulator;

import javax.swing.*;
import java.awt.*;

public class panelConfiguration {
    //Rotulo que apresenta entrada de memoria principal
    private JLabel apresenta_entrada_mp;
    //Entrada de memoria principal
    private JTextField entrada_mp;
    //Painel que contem componentes sobre a mp
    private JPanel painel_mp;
    //Rotulo que apresenta entrada de memoria secundaria
    private JLabel apresenta_entrada_disco;
    //Entrada de memoria secundaria
    private JTextField entrada_disco;
    //Painel que contem componentes sobre o disco
    private JPanel painel_disco;
    //Rotulo que apresenta entrada de tamanho de quadros
    private JLabel apresenta_tamanho_quadro;
    //Entrada de tamanho dos quadros
    private JTextField tamanho_quadros;
    //Painel que contem componentes sobre o tamanho dos quadros
    private JPanel painel_tamanho_quadros;
    //Rotulo que apresenta entrada de tamanho do endereço lógico
    private JLabel apresenta_endereco_logico;
    //Entrada de tamanho do endereço lógico
    private JTextField endereco_logico;
    //Painel que contem componentes sobre o tamanho do endereço
    private JPanel painel_tamanho_end_logico;
    //Painel onde as informações de entrada estarão
    private JPanel caixa_entrada;
    //Painel que representa a janela
    private JPanel janela_config;
    //Botão que leva a execução
    private JButton salvar;
    //Rotulo que avisa do erro
    private JLabel erro;
    //Guarda janela onde o painel está
    private JFrame janela_mae;
    //Valor booleano que guarda se dados de entrada são válidos
    private boolean[] dados_validos;
    //Objeto que guarda as configurações do sistema
    private Configuration configs;
    //Opções de seleção de política de substituição de páginas
    private JComboBox politica_substituicao;
    //Apresenta política de substituição
    private JLabel apresenta_substituicao;
    //Painel onde o rotulo e a caixa de seleção para substituição estarão
    private JPanel painel_substituicao;


    public panelConfiguration(JFrame janela){
        //Instancia vetor de booleanos para validação de dados
        dados_validos = new boolean[4];
        //Dados validas inicia como falso
        for(int i=0; i<4; i++) dados_validos[i] = false;
        //Instancia objeto que guarda configurações
        configs = new Configuration();

        //Guarda janela mae onde o painel esta
        janela_mae = janela;
        //instancia painel que representa a janela
        janela_config = new JPanel(new BorderLayout());
        //Atribui tamanho a ela
        janela_config.setPreferredSize(new Dimension(350, 240));

        instanciaCaixaEntrada();

        instanciaBotao();

        //Instancia rotulo de erro e o adiciona no painel
        erro = new JLabel("inteiro seguido de b/k/m/g");
        caixa_entrada.add(erro);

        pintaPaineis();
    }


    public JPanel getJanelaConfig(){
        return janela_config;
    }

    private void instanciaCaixaEntrada(){
        //instancia caixa onde as entradas estarão
        caixa_entrada = new JPanel(new FlowLayout());
        //atribui tamanho a ela
        caixa_entrada.setPreferredSize(new Dimension(350, 240));
        instanciaEntradasERotulos();
        //Adiciona no centro do painel de janela
        janela_config.add(caixa_entrada, BorderLayout.LINE_START);
    }

    private void instanciaEntradasERotulos(){
        //instancia apresentação de entrada de MP
        apresenta_entrada_mp = new JLabel("Tamanho da MP:");
        //instancia entrada de tamanho de MP
        entrada_mp = new JTextField(16);
        //Instancia painel com componentes sobre MP
        painel_mp = new JPanel(new FlowLayout());
        //Atribui tamanho ao painel de memoria principal
        painel_mp.setPreferredSize(new Dimension(350, 30));
        //Adiciona os componentes sobre MP ao painel da MP
        painel_mp.add(apresenta_entrada_mp);
        painel_mp.add(entrada_mp);
        //Adiciona o painel na caixa de entrada
        caixa_entrada.add(painel_mp);

        //instancia apresentação de entrada de Disco
        apresenta_entrada_disco = new JLabel("Tamanho do Disco:");
        //instancia entrada de tamanho de Disco
        entrada_disco = new JTextField(14);
        //Instancia painel com componentes sobre Disco
        painel_disco = new JPanel(new FlowLayout());
        //Atribui tamanho ao painel de memoria secundária
        painel_disco.setPreferredSize(new Dimension(350, 30));
        //Adiciona os componentes sobre Disco ao painel do Disco
        painel_disco.add(apresenta_entrada_disco);
        painel_disco.add(entrada_disco);
        //Adiciona o painel na caixa de entrada
        caixa_entrada.add(painel_disco);

        //instancia apresentação de tamanho dos quadros
        apresenta_tamanho_quadro = new JLabel("Tamanho dos Quadros:");
        //instancia entrada de tamanho dos quadros
        tamanho_quadros = new JTextField(12);
        //Instancia painel com componentes sobre Tamanho dos quadros
        painel_tamanho_quadros = new JPanel(new FlowLayout());
        //Atribui tamanho ao painel de tamanho dos quadros
        painel_tamanho_quadros.setPreferredSize(new Dimension(350, 30));
        //Adiciona os componentes sobre quadros ao painel dos quadros
        painel_tamanho_quadros.add(apresenta_tamanho_quadro);
        painel_tamanho_quadros.add(tamanho_quadros);
        //Adiciona o painel na caixa de entrada
        caixa_entrada.add(painel_tamanho_quadros);

        //instancia apresentação de tamanho do endereço lógico
        apresenta_endereco_logico = new JLabel("Tamanho do end logico:");
        //instancia entrada de tamanho do endereço lógico
        endereco_logico = new JTextField(11);
        //Instancia painel com componentes sobre tamanho do endereço lógico
        painel_tamanho_end_logico = new JPanel(new FlowLayout());
        //Atribui tamanho ao painel de endereço lógico
        painel_tamanho_end_logico.setPreferredSize(new Dimension(350, 30));
        //Adiciona os componentes sobre endereço lógico ao painel do endereço lógico
        painel_tamanho_end_logico.add(apresenta_endereco_logico);
        painel_tamanho_end_logico.add(endereco_logico);
        //Adiciona o painel na caixa de entrada
        caixa_entrada.add(painel_tamanho_end_logico);

        //Instancia apresentação da política de substituição
        apresenta_substituicao = new JLabel("Politica de substituicao:");
        //Instancia as opções de política
        String[] opcoes = {"LRU", "Clock"};
        //instancia caixa de seleção para políticas de substituição
        politica_substituicao = new JComboBox(opcoes);
        //Instancia painel onde o rotulo e caixa estarão
        painel_substituicao = new JPanel(new FlowLayout());
        //Atribui tamanho ao painel de substituição
        painel_substituicao.setPreferredSize((new Dimension(350, 30)));
        //Adiciona os elementos a ele
        painel_substituicao.add(apresenta_substituicao);
        painel_substituicao.add(politica_substituicao);
        //Adiciona o painel a caixa de entrada
        caixa_entrada.add(painel_substituicao);
    }

    private void pintaPaineis() {
        //Pinta todos os 5 paineis
        painel_tamanho_end_logico.setBackground(new Color(30, 163, 225));
        painel_tamanho_quadros.setBackground(new Color(30, 163, 225));
        painel_mp.setBackground(new Color(30, 163, 225));
        painel_disco.setBackground(new Color(30, 163, 225));
        caixa_entrada.setBackground(new Color(30, 163, 225));
        painel_substituicao.setBackground(new Color(30, 163, 225));
    }

    private void instanciaBotao() {
        //Instancia botão e adiciona no painel de entradas
        salvar = new JButton("salvar");
        caixa_entrada.add(salvar);

        //Ao clicar no botão
        salvar.addActionListener(new java.awt.event.ActionListener(){
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                //Checa os dados, se válidos forem válidos, salva também
                dados_validos[0] = validaDadosU(entrada_mp.getText(), 0);
                dados_validos[1] = validaDadosU(entrada_disco.getText(), 1);
                dados_validos[2] = validaDadosU(endereco_logico.getText(), 2);
                dados_validos[3] = validaDadosU(tamanho_quadros.getText(), 3);
                //Se todas as entradas são válidas
                if(dados_validos[0]&&dados_validos[1]&&dados_validos[2]&&dados_validos[3]){
                    String pol = (String)politica_substituicao.getSelectedItem();
                    //Recebe política de substituição
                    configs.setPoliticaSubstituicao(pol);
                    //Fecha a janela de configuração
                    janela_mae.dispose();
                    //Instancia controlador
                    Controlador controlador = new Controlador(configs.getTamMemPrincipal(), configs.getTamEndLogico(),
                            configs.getTamQuadro(), "ArquivosTeste/TesteSub/criaProc.txt", 50,
                            configs.getTamMemSecundaria(), configs.getPoliticaSubstituicao());
                    //Abre a janela que monitora a execução dos processos
                    Main.emulatorGUI(controlador);
                }
                //Se algum dado não é válido
                else erro.setText("Entrada(s) invalida(s)");
            }
        });
    }

    private boolean validaDadosU(String entrada, int flag) {
        //Cria string para armazenar tamanho
        String tamanho = "";
        //armazena cada caractere lido durante o laço
        char caractere;
        //armazna unidade da entrada
        char unidade;
        //Se a string de entrada for vazia
        if(entrada.equals("")) return false;
        //Percorre a string de entrada
        for(int i=0; i<entrada.length(); i++) {
            //Recebe caractere da posição atual do laço
            caractere = entrada.charAt(i);
            //Testa pra saber se é um número
            if(caractere=='1' || caractere=='2' || caractere=='3' || caractere=='4' ||
                    caractere=='5' || caractere=='6' || caractere=='7' || caractere=='8'
                    || caractere=='9' || caractere=='0') {
                tamanho += caractere;
                //Se a flag indicar que a entrada é endereço lógico
                if(flag == 2) {
                    //Se terminou de ler a entrada de caracteres
                    if(i == entrada.length()-1) {
                        configs.setTamEnd(Integer.parseInt(tamanho));
                        return true;
                    }
                }
            }
            //Testa para receber unidade
            else if(caractere=='k' || caractere=='m' || caractere=='g' || caractere=='b'){
                //Se a unidade vier primeiro
                if(i == 0) return false;
                unidade =  caractere;
                //Se a flag indicar que a entrada é da MP
                if(flag == 0) {
                    configs.setTamMP(Integer.parseInt(tamanho));
                    configs.setU_mp(unidade);
                }
                //Se a flag indicar que a entrada é Disco
                if(flag == 1) {
                    configs.setTamMDisco(Integer.parseInt(tamanho));
                    configs.setU_disco(unidade);
                }
                //Se a flag indicar que a entrada é tamanho do quadr
                if(flag == 3) {
                    configs.setTamQuad(Integer.parseInt(tamanho));
                    configs.setU_quadro(unidade);
                }
                //Como a unidade é a última coisa a ser lida
                return true;
            }
            //Se não recebeu algarismo nem unidade
            else return false;
        }

        //Os casos esperados são tratados dentro do laço, se saiu do laço, então...
        return false;
    }

}
