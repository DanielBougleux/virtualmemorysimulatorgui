package com.os_emulator;

import javax.swing.*;
import java.awt.*;


public class Main {

    public static void main(String[] args) {
        //Método que roda a janela em loop
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                configurationGUI();
            }
        });

    }

    public static void configurationGUI(){
        //Cria objeto janela e atribui titulo
        JFrame janela = new JFrame("OS Configuration");
        //Impede de alterar seu tamanho
        janela.setResizable(false);
        //Mantem o programa rodando ao fechar a janela
        janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Instancia painel de configuração
        panelConfiguration painel = new panelConfiguration(janela);
        //Adicionao painel de configuração a janela
        janela.getContentPane().add(painel.getJanelaConfig());
        //Dimensiona a janela com o gerenciador de layout default
        janela.pack();
        //Torna a janela visivel
        janela.setVisible(true);
    }

    public static void emulatorGUI(Controlador controlador_exec) {
        //Cria objeto janela e atribui titulo
        JFrame janela = new JFrame("OS Emulator");
        //Impede de alterar seu tamanho
        janela.setResizable(false);
        //Termina o programa ao fechar a janela
        janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Cria um objeto controlador de painel
        panelController controlador = new panelController(controlador_exec);
        //Atribui layout em bordas para a janela
        janela.setLayout(new BorderLayout());
        //Adiciona o painel superior a janela
        janela.getContentPane().add(controlador.getPainelSuperior(), BorderLayout.NORTH);
        //Adiciona o painel inferior a janela
        janela.getContentPane().add(controlador.getPainelInferior(), BorderLayout.SOUTH);
        //Adiciona o painel lateral a janela
        janela.getContentPane().add(controlador.getPainelCentral(), BorderLayout.WEST);
        //Dimensiona a janela com o gerenciador de layout default
        janela.pack();
        //Torna a janela visivel
        janela.setVisible(true);
        //Atribui o controlador de paineis ao controlador de execução
        controlador_exec.setPainelPrincipal(controlador);
    }

}
