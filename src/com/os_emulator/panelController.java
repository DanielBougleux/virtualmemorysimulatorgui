package com.os_emulator;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.util.Vector;

public class panelController {
    //Lista de tabela de paginas
    private JTable tabela_paginas;
    //Modelo da tabela de páginas
    private DefaultTableModel modelo_paginas;
    //Lista com o nome das colunas da tabela de paginas
    private Object[] colunas_paginas;
    //Painel superior onde as listas de paginas e processos estarão
    private JSplitPane painel_superior;
    //Scroller para tabela de paginas
    private JScrollPane paginas_scroller;
    //Lista de processos e suas informações
    private JTable tabela_processos;
    //Modelo para a tabela de processos
    private DefaultTableModel modelo_processos;
    //Lista com o nome das colunas da tabela de processos
    private Object[] colunas_processos;
    //Scroller para tabela de processos
    private JScrollPane processos_scroller;
    //Painel inferior onde as informações da MP, Disco e processo em execução estarão
    private JSplitPane painel_inferior;
    //Painel que contém as informações da MP e Disco
    private JPanel painel_memoria;
    //Painel que contém as informações do processo e botões de execução
    private JPanel painel_processo;
    //Painel onde as informações da memória principal estarão
    private JPanel memoria_principal;
    //Painel onde as informações do disco estarão
    private JPanel disco;
    //Painel que contém detalhes do processo em execução
    private JPanel detalhes_processo;
    //Painel que contém botões para gerenciar execução do programa
    private JPanel execucao;
    //Botao de ler proxima linha do arquivo
    private JButton proxima;
    //Botao de play/pause na leitura do arquivo
    private JButton play_pause;
    //Rotulo de aviso sobre eventos relacionados a execucao
    private JLabel aviso;
    //Rotulo que apresenta quadros livres
    private JLabel apresenta_quadros_livres;
    //Rotulo com numero de quadros livres
    private JLabel quadros_livres;
    //Painel com rotulos de quadros livres
    private JPanel painel_quadros_livres;
    //Rotulo que apresenta paginas alocadas
    private JLabel apresenta_paginas_alocadas;
    //Rotulo com número de paginas alocadas
    private JLabel paginas_alocadas;
    //Painel com rotulos sobre paginas alocadas
    private JPanel painel_paginas_alocadas;
    //Rotulo que apresenta memoria livre em disco
    private JLabel apresenta_disco_livre;
    //Rotulo com quantidade de memoria livre em disco
    private JLabel disco_livre;
    //Painel com os rotulos de disco livre
    private JPanel painel_disco_livre;
    //Rotulo que apresenta memoria alocada em disco
    private JLabel apresenta_disco_usado;
    //Rotulo com quantidade de memoria alocada em disco
    private JLabel disco_usado;
    //Painel com os rotulos de disco usado
    private JPanel painel_disco_usado;
    //Rotulo que apresenta identificação do processo
    private JLabel apresenta_identificacao;
    //Rotulo que contem identificacao do processo
    private JLabel identificacao;
    //Painel que contem rotulos sobre identificacao do processo
    private JPanel painel_identificacao;
    //Rotulo que apresenta contador de page fault
    private JLabel apresenta_page_fault;
    //Rotulo que conta pages fault do processo
    private JLabel page_fault;
    //Painel que contem rotulos sobre page fault do processo
    private JPanel painel_page_fault;
    //Rotulo que apresenta memoria alocada para o processo na MP
    private JLabel apresenta_memoria_alocada;
    //Rotulo que conta memoria alocada do processo
    private JLabel memoria_alocada;
    //Painel que contem rotulos sobre memoria alocada do processo
    private JPanel painel_memoria_alocada;
    //Rotulo que apresenta memoria total do processo em disco
    private JLabel apresenta_memoria_total;
    //Rotulo que contem memoral total do processo
    private JLabel memoria_total;
    //Painel que contem rotulos sobre memoria total do processo
    private JPanel painel_memoria_total;
    //Painel em que TLB e quadros da MP estarão:
    private JSplitPane painel_central;
    //Colunas da tabela de TLB
    private Object[] colunas_tlb;
    //Modelo para a tabela TLB
    private DefaultTableModel modelo_tlb;
    //Tabela da TLB
    private JTable tabela_tlb;
    //O scroller para tornar o tamanho da tabela dinâmico
    private JScrollPane scroller_tlb;
    //Colunas da tabela de quadros da MP
    private Object[] colunas_quadros;
    //Modelo para tabela de quadros
    private DefaultTableModel modelo_quadros;
    //Tabela de quadros da MP
    private JTable tabela_quadros;
    //O scroller para tornar o tamanho da tabela dinâmico
    private JScrollPane scroller_quadros;
    //Painel que mostra último endereço acessado
    private JPanel painel_endereco_acessado;
    //Rotulo que apresenta último endereço acessado
    private JLabel apresenta_ultimo_endereco;
    //Mostra qual foi último endereço físico acessado
    private JLabel ultimo_endereco;
    //Painel que mostra última instrução executada
    private JPanel painel_ultima_instrucao;
    //Rotulo que apresenta última instrução
    private JLabel apresenta_ultima_instrucao;
    //Mostra qual foi a última instrução
    private JLabel ultima_instrucao;
    //Controlador de execução
    private Controlador controlador;



    public panelController(Controlador control){
        //Recebe o controlador
        controlador = control;

        //Instancia painel superior
        instanciaPainelSuperior();

        //Instancia painel inferior
        instanciaPainelInferior();

        //Instancia painel lateral
        instanciaPainelCentral();

        //Adiciona borda e cor de fundo aos paineis
        adicionaBordasPaineis();
        pintaPaineis();
    }

    //Getter para o painel superior
    public JSplitPane getPainelSuperior() {
        return painel_superior;
    }

    //Getter para o painel inferior
    public JSplitPane getPainelInferior() {
        return painel_inferior;
    }

    //Getter para o painel lateral
    public JSplitPane getPainelCentral() { return painel_central; }

    private void instanciaPainelSuperior() {
        //Instancia lista de colunas para tabela de páginas
        colunas_paginas = new Object[]{"presente", "modificada", "#quadro", "#pagina"};

        //Instacia lista de paginas
        tabela_paginas = new JTable();
        //Instancia modelo para a tabela
        modelo_paginas = new DefaultTableModel();
        modelo_paginas.setColumnIdentifiers(colunas_paginas);
        //Adiciona a coluna ao modelo da tabela
        tabela_paginas.setModel(modelo_paginas);
        //Permite selecionar apenas por linha
        tabela_paginas.setRowSelectionAllowed(true);
        tabela_paginas.setColumnSelectionAllowed(false);

        //Instancia ponteiro para as colunas
        TableColumn column_pages = null;
        //Percorre as 3 colunas
        for (int i = 0; i < 4; i++) {
            //Pega a coluna atual da iteração
            column_pages = tabela_paginas.getColumnModel().getColumn(i);
            //Quadros
            if (i == 2) {
                column_pages.setPreferredWidth(155);
            }
            //Presente
            else if (i == 0) {
                column_pages.setPreferredWidth(70);
            }
            //Modificado
            else if(i == 1){
                column_pages.setPreferredWidth(90);
            }
            //Pagina
            else {
                column_pages.setPreferredWidth(150);
            }
        }

        //O Scroller que torna o tamanho da lista de páginas dinâmico
        paginas_scroller = new JScrollPane(tabela_paginas);
        //Tamanho ao ser criado
        paginas_scroller.setPreferredSize(new Dimension(470, 200));

        //Instancia lista de colunas para tabela de processos
        colunas_processos = new Object[]{"processo", "estado"};

        //Instacia lista de processos
        tabela_processos = new JTable();
        modelo_processos = new DefaultTableModel();
        //Atribui as colunas ao modelo
        modelo_processos.setColumnIdentifiers(colunas_processos);
        //Atribui modelo a tabela
        tabela_processos.setModel(modelo_processos);
        //Permite selecionar apenas por linha
        tabela_processos.setRowSelectionAllowed(true);
        tabela_processos.setColumnSelectionAllowed(false);

        //O Scroller que torna o tamanho da lista de processos dinâmico
        processos_scroller = new JScrollPane(tabela_processos);
        //Tamanho ao ser criado
        processos_scroller.setPreferredSize(new Dimension(230, 200));

        //Instancia o painel superior com uma divisoria horizontal
        painel_superior = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                paginas_scroller, processos_scroller);

        //Atribui onde a divisória vai estar
        painel_superior.setDividerLocation(470);
        //Torna a divisória "invisível"
        painel_superior.setDividerSize(0);

        //Atribui tamanho do painel superior ao ser criado
        painel_superior.setPreferredSize(new Dimension(701, 200));
    }

    private void instanciaPainelInferior(){
        //Instancia painel de detalhes do processo
        detalhes_processo = new JPanel(new FlowLayout());
        //Instancia painel de botões de execucao
        execucao = new JPanel(new FlowLayout());

        //Atribui dimensão para o painel de detalhes do processo
        detalhes_processo.setPreferredSize(new Dimension(350, 150));
        //Atribui dimensão para o painel de botões de execução
        execucao.setPreferredSize(new Dimension(350, 50));

        //Instancia painel relacionado ao processo
        painel_processo = new JPanel(new FlowLayout());

        //Adiciona o painel dos botões e dos detalhes
        painel_processo.add(detalhes_processo);
        painel_processo.add(execucao);

        //Atribui dimensão ao painel de processo
        painel_processo.setPreferredSize(new Dimension(350, 200));

        //Instancia painel de memória principal
        memoria_principal = new JPanel(new FlowLayout());
        //Instancia painel de disco
        disco = new JPanel(new FlowLayout());

        //Insere rotulos no painel de memoria principal
        instanciaRotulosMP();

        //Insere rotulos no painel de disco
        instanciaRotulosDisco();

        //Insere rotulos no painel de detalhes do processo
        instanciaRotulosProcesso();

        //Insere botões para gerenciar execucao do simulador
        instanciaBotoesExecucao();

        //Atribui dimensão para os paineis de memória principal e disco
        memoria_principal.setPreferredSize(new Dimension(175, 200));
        disco.setPreferredSize(new Dimension(175, 200));

        //Instancia painel relaciona as memórias
        painel_memoria = new JPanel(new BorderLayout());

        //Adiciona paineis de memória principal e disco no painel de memória
        painel_memoria.add(memoria_principal, BorderLayout.WEST);
        painel_memoria.add(disco, BorderLayout.EAST);

        //Atribui dimensões para o painel de memória
        painel_memoria.setPreferredSize(new Dimension(350, 200));

        //Instancia o painel inferior com uma divisoria horizontal
        painel_inferior = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                painel_memoria, painel_processo);

        //Atribui onde a divisória vai estar
        painel_inferior.setDividerLocation(350);
        //Torna a divisória "invisível"
        painel_inferior.setDividerSize(0);

        //Atribui tamanho do painel inferior ao ser criado
        painel_inferior.setPreferredSize(new Dimension(701, 200));
    }

    private void instanciaPainelCentral() {
        //Instancia lista de colunas para tabela de páginas
        colunas_tlb = new Object[]{"valido","#processo","#pagina","#quadro","modificada","presente"};

        //Instacia tabela tlb
        tabela_tlb = new JTable();
        //Instancia modelo para tabela
        modelo_tlb = new DefaultTableModel();
        modelo_tlb.setColumnIdentifiers(colunas_tlb);
        tabela_tlb.setModel(modelo_tlb);
        //Permite selecionar apenas por linha
        tabela_tlb.setRowSelectionAllowed(true);
        tabela_tlb.setColumnSelectionAllowed(false);

        //Instancia ponteiro para as colunas
        TableColumn column = null;
        //Percorre as 6 colunas
        for (int i = 0; i < 6; i++) {
            //Pega a coluna atual da iteração
            column = tabela_tlb.getColumnModel().getColumn(i);
            //Vaĺido
            if (i == 0) {
                column.setPreferredWidth(50);
            }
            //Processo
            else if(i == 1) {
                column.setPreferredWidth(80);
            }
            //Página e quadro
            else if (i == 2 || i == 3) {
                column.setPreferredWidth(115);
            }
            //Modificado
            else if (i == 4){
                column.setPreferredWidth(85);
            }
            //Presente
            else if (i == 5){
                column.setPreferredWidth(65);
            }
        }

        //O Scroller que torna o tamanho da tabela tlb dinâmico
        scroller_tlb = new JScrollPane(tabela_tlb);
        //Tamanho ao ser criado
        scroller_tlb.setPreferredSize(new Dimension(500, 200));


        //Instancia lista de colunas para tabela de quadros
        colunas_quadros = new Object[]{"#quadro","#proc"};

        //Instacia tabela de quadros
        tabela_quadros = new JTable();
        //Instancia modelo de tabela
        modelo_quadros = new DefaultTableModel();
        //Atribui coluna ao modelo
        modelo_quadros.setColumnIdentifiers(colunas_quadros);
        //Atribui modelo a tabela
        tabela_quadros.setModel(modelo_quadros);
        //Permite selecionar apenas por linha
        tabela_quadros.setRowSelectionAllowed(true);
        tabela_quadros.setColumnSelectionAllowed(false);

        //Instancia ponteiro para as colunas
        TableColumn column_frames = null;
        //Percorre as 2 colunas
        for (int i = 0; i < 2; i++) {
            //Pega a coluna atual da iteração
            column_frames = tabela_quadros.getColumnModel().getColumn(i);
            //Quadros
            if (i == 0) {
                column_frames.setPreferredWidth(120);
            }
            //Processo
            else if (i == 1) {
                column_frames.setPreferredWidth(80);
            }
        }

        //O Scroller que torna o tamanho da tabela de quadros dinâmico
        scroller_quadros = new JScrollPane(tabela_quadros);
        //Tamanho ao ser criado
        scroller_quadros.setPreferredSize(new Dimension(200, 200));

        //Instancia o painel lateral
        painel_central = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                scroller_tlb, scroller_quadros);

        //Atribui onde a divisória vai estar
        painel_central.setDividerLocation(500);
        //Torna a divisória "invisível"
        painel_central.setDividerSize(0);

        //Atribui tamanho do painel ao ser criado
        painel_central.setPreferredSize(new Dimension(701, 200));
    }

    private void instanciaRotulosMP(){
        //Instancia painel de quadros livres
        painel_quadros_livres = new JPanel(new FlowLayout());
        //Atribui tamanho a ele
        painel_quadros_livres.setPreferredSize(new Dimension(155, 40));

        //Instancia apresentação de quadros livres
        apresenta_quadros_livres = new JLabel("Quadros livres:");
        //Insere no painel de quadros livres
        painel_quadros_livres.add(apresenta_quadros_livres);

        //Instancia enumeração de quadros livres
        quadros_livres = new JLabel("0");
        //Insere no painel de quadros livres
        painel_quadros_livres.add(quadros_livres);

        //Insere painel de quadros livres em painel de memoria principal
        memoria_principal.add(painel_quadros_livres);

        //Instancia painel de páginas alocadas
        painel_paginas_alocadas = new JPanel(new FlowLayout());
        //Atribui tamanho a ele
        painel_paginas_alocadas.setPreferredSize(new Dimension(155, 40));

        //Instancia apresentação de paginas alocadas
        apresenta_paginas_alocadas = new JLabel("Pag alocadas:");
        //Insere no painel de paginas alocadas
        painel_paginas_alocadas.add(apresenta_paginas_alocadas);

        //Instancia enumeração de paginas alocadas
        paginas_alocadas = new JLabel("0");
        //Insere no painel de páginas alocadas
        painel_paginas_alocadas.add(paginas_alocadas);

        //Insere painel de páginas alocadas em painel de memoria principal
        memoria_principal.add(painel_paginas_alocadas);


        //Instancia painel de último endereço acessado
        painel_endereco_acessado = new JPanel(new FlowLayout());
        //Atribui tamanho a ele
        painel_endereco_acessado.setPreferredSize(new Dimension(160, 50));

        //Instancia apresentação de último endereço acessado
        apresenta_ultimo_endereco = new JLabel("Ult end:");
        //Insere no painel de último endereço acessado
        painel_endereco_acessado.add(apresenta_ultimo_endereco);

        //Instancia qual endereço foi acessado por último
        ultimo_endereco = new JLabel("0");
        //Insere no painel de último endereço acessado
        painel_endereco_acessado.add(ultimo_endereco);

        //Insere painel de último endereço acessado em painel de memoria principal
        memoria_principal.add(painel_endereco_acessado);
    }

    private void instanciaRotulosDisco() {
        //Instancia painel de disco livre
        painel_disco_livre = new JPanel(new FlowLayout());
        //Atribui tamanho a ele
        painel_disco_livre.setPreferredSize(new Dimension(155, 40));

        //Instancia apresentação de disco livre
        apresenta_disco_livre = new JLabel("Disco livre:");
        //Insere no painel de disco livre
        painel_disco_livre.add(apresenta_disco_livre);

        //Instancia enumeração de disco livre
        disco_livre = new JLabel("0");
        //Insere no painel de disco livre
        painel_disco_livre.add(disco_livre);

        //Insere painel de disco livre em painel de disco
        disco.add(painel_disco_livre);

        //Instancia painel de disco usado
        painel_disco_usado = new JPanel(new FlowLayout());
        //Atribui tamanho a ele
        painel_disco_usado.setPreferredSize(new Dimension(155, 40));

        //Instancia apresentação de disco usado
        apresenta_disco_usado = new JLabel("Disco usado:");
        //Insere no painel de disco usado
        painel_disco_usado.add(apresenta_disco_usado);

        //Instancia enumeração de disco usado
        disco_usado = new JLabel("0");
        //Insere no painel de disco usado
        painel_disco_usado.add(disco_usado);

        //Insere painel de disco usado em painel de disco
        disco.add(painel_disco_usado);
    }

    private void instanciaRotulosProcesso() {
        //Instancia painel de identificação do processo
        painel_identificacao = new JPanel(new FlowLayout());
        //Atribui tamanho a ele
        painel_identificacao.setPreferredSize(new Dimension(155, 40));

        //Instancia apresentação de identificação do processo
        apresenta_identificacao = new JLabel("Identificacao:");
        //Insere no painel de identificação do processo
        painel_identificacao.add(apresenta_identificacao);

        //Instancia identificação do processo
        identificacao = new JLabel("P#");
        //Insere no painel de identificação
        painel_identificacao.add(identificacao);

        //Insere painel de identificação em painel de detalhes do processo
        detalhes_processo.add(painel_identificacao);

        //Instancia painel de page fault
        painel_page_fault = new JPanel(new FlowLayout());
        //Atribui tamanho a ele
        painel_page_fault.setPreferredSize(new Dimension(155, 40));

        //Instancia apresentação de page fault
        apresenta_page_fault = new JLabel("Page fault:");
        //Insere no painel de page fault
        painel_page_fault.add(apresenta_page_fault);

        //Instancia enumeração de page fault
        page_fault = new JLabel("0");
        //Insere no painel de page fault
        painel_page_fault.add(page_fault);

        //Insere painel de page fault em painel de detalhes do processo
        detalhes_processo.add(painel_page_fault);

        //Instancia painel de memoria alocada do processo
        painel_memoria_alocada = new JPanel(new FlowLayout());
        //Atribui tamanho a ele
        painel_memoria_alocada.setPreferredSize(new Dimension(155, 40));

        //Instancia apresentação de memoria alocada
        apresenta_memoria_alocada = new JLabel("Mem alocada:");
        //Insere no painel de memoria alocada do processo
        painel_memoria_alocada.add(apresenta_memoria_alocada);

        //Instancia contador de memoria alocada
        memoria_alocada = new JLabel("0");
        //Insere no painel de memoria alocada do processo
        painel_memoria_alocada.add(memoria_alocada);

        //Insere painel de memoria alocada em painel de detalhes do processo
        detalhes_processo.add(painel_memoria_alocada);

        //Instancia painel de memoria total em disco
        painel_memoria_total = new JPanel(new FlowLayout());
        //Atribui tamanho a ele
        painel_memoria_total.setPreferredSize(new Dimension(155, 40));

        //Instancia apresentação de memoria total em disco
        apresenta_memoria_total = new JLabel("Mem total:");
        //Insere no painel de memoria total em disco
        painel_memoria_total.add(apresenta_memoria_total);

        //Instancia enumeração de memoria total
        memoria_total = new JLabel("0");
        //Insere no painel de memoria total em disco
        painel_memoria_total.add(memoria_total);

        //Insere painel de memoria total em painel de detalhes do processo
        detalhes_processo.add(painel_memoria_total);

        //Instancia painel de última instrução executada
        painel_ultima_instrucao = new JPanel(new FlowLayout());
        //Atribui tamanho a ele
        painel_ultima_instrucao.setPreferredSize(new Dimension(185, 30));

        //Instancia apresentação de última instrução executada
        apresenta_ultima_instrucao = new JLabel("Ult intrucao:");
        //Insere no painel de identificação do processo
        painel_ultima_instrucao.add(apresenta_ultima_instrucao);

        //Instancia última instrução executada
        ultima_instrucao = new JLabel("XX 00");
        //Insere no painel de identificação
        painel_ultima_instrucao.add(ultima_instrucao);

        //Insere painel de última instrução executada em painel de detalhes do processo
        detalhes_processo.add(painel_ultima_instrucao);
    }

    private void instanciaBotoesExecucao() {
        //Instancia botão de aviso, inicialmente não há nada para avisar
        aviso = new JLabel("");
        execucao.add(aviso);

        //Instancia botão e adiciona ao painel de execução
        proxima = new JButton("proxima");
        execucao.add(proxima);

        //Ao clicar no botão de ler próxima linha do arquivo
        proxima.addActionListener(new java.awt.event.ActionListener(){
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                //Se a execução não estiver em modo automático
                if(play_pause.getText().equals("play")) {
                    //lê próxima linha
                    controlador.loop();
                }
            }
        });

        //Instancia botão e adiciona ao painel de execução
        play_pause = new JButton("play");
        execucao.add(play_pause);

        //Ao clicar no botão de leitura automática do arquivo
        play_pause.addActionListener(new java.awt.event.ActionListener(){
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                //Se a execução não estiver em modo automático
                if(play_pause.getText().equals("play")) {
                    //Contador de tempo para automação
                    long tempo_inicio = System.currentTimeMillis();
                    //Tempo para chegar a próxima leitura de instrução
                    long tempo_exec = System.currentTimeMillis();
                    //Entra no modo automático da leitura do arquivo
                    for(int i = 0; i<5; i++){
                        //Espaço de 2 segundos entre cada instrução
                        while(tempo_exec - tempo_inicio < 2000) {
                            tempo_exec = System.currentTimeMillis();
                        }
                        //lê próxima linha
                        controlador.loop();
                        //Atualiza temporizador
                        tempo_inicio =  System.currentTimeMillis();
                    }
                    //Atualiza o texto do botão, dizendo que entrou em modo automático
                    play_pause.setText("pause");
                }
                //Se a execução estiver em modo automático
                else if(play_pause.getText().equals("pause")) {
                    //Atualiza o texto do botão, dizendo que saiu do modo automático
                    play_pause.setText("play");
                    //Pausa o modo automático da leitura do arquivo
                }
            }
        });
    }

    private void adicionaBordasPaineis() {
        //Atribui cor e titulo a borda do scroller
        paginas_scroller.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.black, 5),
                "Tabela de paginas"
        ));

        //Atribui cor e titulo a borda do scroller
        processos_scroller.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.black, 5),
                "Lista de processos"
        ));

        //Atribui cor e titulo a borda do scroller
        scroller_tlb.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.black, 5),
                "TLB"
        ));

        //Atribui cor e titulo a borda do scroller
        scroller_quadros.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.black, 5),
                "Quadros"
        ));

        //Atribui cor e titulo a borda do painel
        detalhes_processo.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.black, 5),
                "Processo em execucao"
        ));

        //Atribui cor e titulo as bordas dos paineis
        memoria_principal.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.black, 5),
                "Memoria principal"
        ));
        disco.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.black, 5),
                "Disco"
        ));
    }

    private void pintaPaineis(){
        //Atribui cor de fundo ao scroller
        paginas_scroller.setBackground(new Color(30, 163, 225));
        paginas_scroller.getViewport().setBackground(new Color(70, 205, 245));

        //Atribui cor de fundo ao scroller
        processos_scroller.setBackground(new Color(30, 163, 225));
        processos_scroller.getViewport().setBackground(new Color(70, 205, 245));

        //Atribui cor de fundo para ambas as listas
        tabela_paginas.setBackground(new Color(50, 168, 162));
        tabela_processos.setBackground(new Color(50, 168, 162));
        tabela_paginas.getTableHeader().setBackground(new Color(70, 205, 245));
        tabela_processos.getTableHeader().setBackground(new Color(70, 205, 245));
        tabela_paginas.setSelectionBackground(new Color(235, 235, 235));
        tabela_processos.setSelectionBackground(new Color(235, 235, 235));

        //Atribui cor de fundo ao scroller
        scroller_tlb.setBackground(new Color(30, 163, 225));
        scroller_tlb.getViewport().setBackground(new Color(70, 205, 245));

        //Atribui cor de fundo ao scroller
        scroller_quadros.setBackground(new Color(30, 163, 225));
        scroller_quadros.getViewport().setBackground(new Color(70, 205, 245));

        //Atribui cor de fundo para ambas as listas
        tabela_tlb.setBackground(new Color(50, 168, 162));
        tabela_quadros.setBackground(new Color(50, 168, 162));
        tabela_tlb.getTableHeader().setBackground(new Color(70, 205, 245));
        tabela_quadros.getTableHeader().setBackground(new Color(70, 205, 245));
        tabela_tlb.setSelectionBackground(new Color(235, 235, 235));
        tabela_quadros.setSelectionBackground(new Color(235, 235, 235));

        //Atribui cor de fundo aos paineis
        detalhes_processo.setBackground(new Color(30, 163, 225));
        execucao.setBackground(new Color(30, 163, 225));

        //Atribui cor de fundo ao painel de processo
        painel_processo.setBackground(new Color(30, 163, 225));

        //Atribui cor de fundo aos paineis
        memoria_principal.setBackground(new Color(30, 163, 225));
        disco.setBackground(new Color(30, 163, 225));

        //Atribui cor de fundo ao painel de memoria
        painel_memoria.setBackground(new Color(30, 163, 225));

        //Atribui cor de fundo ao painel de identificação
        painel_identificacao.setBackground(new Color(30, 163, 225));

        //Atribui cor de fundo ao painel de page fault
        painel_page_fault.setBackground(new Color(30, 163, 225));

        //Atribui cor de fundo ao painel de memoria alocada do processo
        painel_memoria_alocada.setBackground(new Color(30, 163, 225));

        //Atribui cor de fundo ao painel de memoria total do processo
        painel_memoria_total.setBackground(new Color(30, 163, 225));

        //Atribui cor de fundo ao painel de disco usado
        painel_disco_usado.setBackground(new Color(30, 163, 225));

        //Atribui cor de fundo ao painel de disco livre
        painel_disco_livre.setBackground(new Color(30, 163, 225));

        //Atribui cor de fundo ao painel de paginas alocadas
        painel_paginas_alocadas.setBackground(new Color(30, 163, 225));

        //Atribui cor de fundo ao painel de quadros livres
        painel_quadros_livres.setBackground(new Color(30, 163, 225));

        //Atribui cor de fundo ao painel de último endereço acessado
        painel_endereco_acessado.setBackground(new Color(30, 163, 225));

        //Atribui cor de fundo ao painel de última instrução executada
        painel_ultima_instrucao.setBackground(new Color(30, 163, 225));
    }

    //Método que altera valor de quadros livres
    public void setQuadrosLivres(String quadros) {
        quadros_livres.setText(quadros);
    }

    //Método que altera valor de páginas alocadas
    public void setPaginasAlocadas(String paginas) {
        paginas_alocadas.setText(paginas);
    }

    //Método para alterar valor de disco livre
    public void setDiscoLivre(String d_livre) {
        disco_livre.setText(d_livre);
    }

    //Método para alterar valor de disco usado
    public void setDiscoUsado(String d_usado) {
        disco_usado.setText(d_usado);
    }

    //Método para alterar processo em execução
    public void setProcessoExecucao(String p_id) { identificacao.setText(p_id); }

    //Método para alterar contador de page faults do processo
    public void setPageFault(String p_fault) { page_fault.setText(p_fault); }

    //Método para alterar quantidade de memória alocada do processo
    public void setMemAlocadaProcesso(String m_alocada){ memoria_alocada.setText(m_alocada); }

    //Método para alterar memória total do processo
    public void setMemTotal(String m_total) {
        memoria_total.setText(m_total);
    }

    //Método para alterar último endereço acessado
    public void setUltimoEndereco(String end) {
        long end_long = Long.parseLong(end, 2);
        ultimo_endereco.setText(Long.toString(end_long));
    }

    //Método para alterar última instrução executada
    public void setUltimaInstrucao(String inst) { ultima_instrucao.setText(inst); }

    //Método para adicionar linha na tabela de paginas
    public void addPageTable(String p, String m, int q, int pag) {
        Object[] linha = new Object[]{p, m, q, pag};
        modelo_paginas.addRow(linha);
    }

    //Overcharge para adicionar em posição específica
    public void addPageTable(String p, String m, int q) {

    }

    //Método para limpar tabela de páginas
    public void clearPageTable() {
        modelo_paginas.setRowCount(0);
        /*for(int i=0; i<getSizePageTable(); i++) {
            modelo_paginas.removeRow(i);
        }*/
    }

    //Método que retorna tamanho da tabela de páginas
    public int getSizePageTable() {
        return modelo_paginas.getRowCount();
    }

    //Método para remover linha na tabela de páginas
    public void removePageTable(int index) {
        modelo_paginas.removeRow(index);
    }

    //Overcharge para remover através do quadro
    public void removePageTable(String quadro) {
        //Itera pelas linhas da tabela de páginas
        for(int i=0; i<getSizePageTable(); i++) {
            //Se a coluna de quadro for igual ao parametro
            if(modelo_paginas.getValueAt(i,2).equals(quadro)) {
                modelo_paginas.removeRow(i);
                return;
            }
        }
    }


    //Método para adicionar linha na tabela de processos
    public void addProcessTable(String p, String e) {
        Object[] linha = new Object[]{p, e};
        modelo_processos.addRow(linha);
    }

    //Overcharge para adicionar em posição específica
    public void addProcessTable(String p, String e, int index) {
    }

    //Método para limpar tabela de processos
    public void clearProcessTable() {
        modelo_processos.setRowCount(0);
        /*for(int i=0; i<getSizeProcessTable(); i++) {
            modelo_processos.removeRow(i);
        }*/
    }

    //Método que retorna tamanho da tabela de processos
    public int getSizeProcessTable() {
        return modelo_processos.getRowCount();
    }

    //Método para remover linha na tabela de processos
    public void removeProcessTable(int index) {
        modelo_processos.removeRow(index);
    }

    //Overcharge para remover através do processo
    public void removeProcessTable(String processo) {
        //Itera pelas linhas da tabela de processo
        for(int i=0; i<getSizeProcessTable(); i++) {
            //Se a coluna de processo for igual ao parametro
            if(modelo_processos.getValueAt(i, 0).equals(processo)) {
                modelo_processos.removeRow(i);
                return;
            }
        }
    }


    //Método para adicionar linha nos quadros da MP
    public void addFramesMP(String q, String p) {
        Object[] linha = new Object[]{q, p};
        modelo_quadros.addRow(linha);
    }

    //Overcharge para adicionar em posição específica
    public void addFramesMP(String q, String p, int index) {
;
    }

    //Método que retorna quantidade de quadros na MP
    public int getSizeFramesMP() {
        return modelo_quadros.getRowCount();
    }

    //Método para limpar quadros da MP
    public void clearFramesMP() {
        modelo_quadros.setRowCount(0);
        /*for(int i=0; i<getSizeFramesMP(); i++) {
            modelo_quadros.removeRow(i);
        }*/
    }

    //Método para remover linha dos quadros da MP
    public void removeFramesMP(int index) {
        modelo_quadros.removeRow(index);
    }

    //Overcharge para remover através do quadro
    public void removeFramesMP(String quadro) {
        //Itera pelas linhas dos quadros da MP
        for(int i=0; i<getSizeFramesMP(); i++) {
            //Se a coluna de quadro for igual ao parametro
            if(modelo_quadros.getValueAt(i, 0).equals(quadro)) {
                modelo_quadros.removeRow(i);
                return;
            }
        }
    }


    //Método para adicionar linha na tabela da TLB
    public void addTLBtable(String v, String pro, String pag, String q,
                                String m, String pre) {
        long q_long = Long.parseLong(q);
        Object[]linha = new Object[]{v,pro,pag,q_long,m,pre};
        modelo_tlb.addRow(linha);
    }

    //Overcharge para adicionar em posição específica
    public void addTLBtable(String v, String pro, String pag, String q,
                                String m, String pre, int index) {
    }

    //Método que retorna numero de linhas na TLB
    public int getSizeTLBtable() {
        return modelo_tlb.getRowCount();
    }

    //Método para limpar tabela TLB
    public void clearTLBtable() {
        modelo_tlb.setRowCount(0);
        /*for(int i=0; i<getSizeTLBtable(); i++) {
            modelo_tlb.removeRow(i);
        }*/
    }

    //Método para remover linha da tabela TLB
    public void removeTLBtable(int index) {
        modelo_tlb.removeRow(index);
    }

    //Overcharge para remover através do quadro
    public void removeTLBtable(String quadro) {
        //Itera pelas linhas da tabela TLB
        for(int i=0; i<getSizeTLBtable(); i++) {
            //Se a coluna de quadro for igual ao parametro
            if(modelo_tlb.getValueAt(i, 3).equals(quadro)) {
                modelo_tlb.removeRow(i);
                return;
            }
        }
    }
}
